/**
 * Users.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

 var bcrypt = require("bcrypt");

module.exports = {

  attributes: {
    username: {
      type: "string"
    },
    email: {
      type: "string"
    },
    password: {
      type: "string"
    },


  },

  
  beforeCreate: function (values, next) {
      // Encrypt the password and record the salt.
      bcrypt.hash(values.password, 10).then(function(hash) {
          // Store hash in your password DB.
          values.password = hash;
          next();
      });
  },

  comparePassword : function (password, user, cb) {
    bcrypt.compare(password, user.password, function (err, match) {

      if(err) cb(err);
      if(match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  }
};

