/**
 * UsersController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


	loginUser: function (req, res) {
		Users.findOne({
			email: req.param("email")
		}).then(user => {

			if(user){
				Users.comparePassword(req.param("password"), user, function (err, valid) {
			        if (err) {
			          return res.json(403, {err: 'forbidden'});
			        }

			        if (!valid) {
			        	req.session.message = "invalid email or password";
			        	return res.redirect("back");
			        } else {
			        	req.session.userId = user.id;
			        	req.session.username = user.username;
			        	req.session.authenticated = true;
			          res.redirect("gems");
			        }
			      });
			}else{
				req.session.message = "invalid email or password";
				return res.redirect("back");
			}
		});
	},
	

	register: function (req, res) {
		Users.create({
			email: req.param("email"),
			username: req.param("username"),
			password: req.param("password"),
			is_admin: 0
		}).exec(function (err, data) {
			if (err) {
				console.log(err)
			}
			return res.redirect("gems");
		})
	},

	login: function (req, res) {
		var message = "";
		if (req.session.message){
			message = req.session.message;
			delete req.session.message;
		}

		return res.view("users/login", {
			message: message
		});
	},

	logout: function (req, res) {
		req.session.authenticated = false;
		return res.redirect("/");
	}
};

