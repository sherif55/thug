/**
 * GemsController
 *
 * @description :: Server-side logic for managing Gems
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	
	index: function (req, res) {
		var message = "";
		if (req.session.gemAdded){
			message = "Thank You! Your gem and dependencies has been added and will be reviewed by on of the admins!"
			delete req.session.gemAdded;
		}

		Gems.find({status: 1}).exec(function (err, data) {
			// console.log(data);
			return res.view("gems/index", {
				gems: data,
				message: message
			});
		})
	},

	view: function (req, res) {
		Gems.findOne(req.param("id")).exec(function (err, gem) {

			var platforms = [1, 2, 3, 4];
			gem.platforms = [];

			GemDependencies.find({gem_id: gem.id})
				.exec(function (err, data) {
					
					if(err)
						return res.serverError(err);

					let depIds = data.map(function (item, index) {
						return item["dependency_id"];
					});

					
					for (var i = 0; i < platforms.length; i++) {
						let platform_id = platforms[i];
						SystemDependencies.find({platform_id: platforms[i], id: depIds})
							.exec(function (err, deps) {
								gem.platforms[platform_id] = deps;
							})

					}
				})


			setTimeout(function () {
				// return res.view(gem);
				return res.view("gems/view", {
					gem: gem
				});
			}, 500);
		})
	},

	create: function (req, res) {
		
		return res.view("gems/create");
	}, 

	store: function (req, res) {
		// return res.redirect("back");
		Gems.create({
			name: req.param("name"),
			url: req.param("url"),
			status: 0,
		}).exec(function (err, gem) {

			if(err)
				return res.serverError(err);
			
			deps = req.param("dependencies");

			for (var i = deps.length - 1; i >= 0; i--) {
				SystemDependencies.create({
					name: deps[i].name,
					platform_id: deps[i].platform
				}).exec(function (err, data) {
					if(err)
						return res.serverError(err);
					GemDependencies.create({
						gem_id: gem.id,
						dependency_id: data.id,
					}).exec(function (err, data) {
						if(err)
							return res.serverError(err);
					})
				})
			}

			req.session.gemAdded = true;
			return res.redirect("/gems");
		})
	}
};

