/**
 * DependenciesController
 *
 * @description :: Server-side logic for managing Dependencies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	// fetches all dependencies of given file
	index: function (req, res) {
		var platform = req.param("platform");

		var gems = req.param("gems");

		// return res.json(gems);
		gems = gems.map(function (item, index) {
			return "'"+item+"'";
		});

		var gemString = gems.join(",");
		var sql = "SELECT * FROM `gems` WHERE name in ("+gemString+")";

		Platforms.findOne({
			name: platform
		}).exec(function (err, plfrm) {

			Gems.query(sql, [], function (err, data) {

				// get all ids of requested gems
				gemIds = data.map(function (item, index) {
					return item["id"];
				});

				var gemString = gemIds.join(",");

				var sql = "SELECT * FROM `gem_dependencies` WHERE gem_id in ("+gemString+")";
				GemDependencies.query(sql, [], function (err, data) {

					sysIds = data.map(function (item, index) {
						return item["dependency_id"];
					});

					sysString = sysIds.join(",");

					if(sysString.length){
						var sql = "SELECT * FROM `system_dependencies` WHERE id in ("+sysString+") AND platform_id = " + plfrm.id;

						SystemDependencies.query(sql, [], function (err, data) {
							res.json(data);
						})
					}else{
						res.json([]);
					}

				})

			});
		});


	}
};

